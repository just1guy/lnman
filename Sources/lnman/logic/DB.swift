//
//  DB.swift
//  
//
//  Created by Robert Sale on 11/24/19.
//

import Foundation
import Pathos
import GRDB

class DB {
    let dbQueue: DatabaseQueue
    
    func getRows(query: String) -> [ [String] ] {
        var data = [[String]]()
        self.dbQueue.read { db in
            let rows = try! Row.fetchAll( db, sql: query )
            var list: [ [String] ] = [ [] ]
            func getCols(_ row: Row) -> [String] {
                var cols: [String] = []
                for i in 0...row.count - 1 {
                    cols.append("\(row[i] ?? 1)")
                }
                return cols
            }
            for row in rows {
                data.append( getCols(row) )
            }
        }
        return data
    }
    
    func getRow(query: String) -> [String: String] {
        var data: [String: String] = [:]
        
        self.dbQueue.read { db in
            let row = try! Row.fetchOne(db, sql: query)
            for (col, value) in row! {
                data[col] = "\(value)"
            }
        }
        return data
    }
    
    func addEntry( query: String ) -> Int64 {
        let result = try! self.dbQueue.write { db -> Int64 in
            try! db.execute( sql: query )
            return db.lastInsertedRowID
        }
        return result
    }
    
    init(_ file: String) {
        self.dbQueue = try! DatabaseQueue(path: file)
    }
}
