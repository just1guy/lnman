//
//  QueryParser.swift
//  
//
//  Created by Robert Sale on 11/24/19.
//

import Foundation

class QueryArgument {
    private var sql: String = ""
    var argName: String
    var sqlString: String {
        get { return sql }
        set(newValue) { sql = "\(argName) \(newValue)" }
    }
    private var params: String = "" {
        didSet {
            sqlString = params
        }
    }
    var paramsString: String {
        get { return params }
        set(newValue) { params = newValue }
    }
    var paramsArray: [String] {
        get { return paramsString.components( separatedBy: "," ) }
        set(newValue) { params = newValue.joined( separator: ",") }
    }
    var paramsSet: Set<String> {
        get {
            var ps: Set<String> = []
            for item in paramsArray {
                ps.insert(item)
            }
            return ps
        }
    }
    
    init(_ argName: String) {
        self.argName = argName
        self.sqlString = ""
    }
}

class QuerySelection: QueryArgument {
    
    override init(_ select: String) {
        super.init("SELECT")
        self.paramsString = select
    }
}

class QueryFrom: QueryArgument {
    
    override init(_ from: String) {
        super.init("FROM")
        self.paramsString = from
    }
}

class QueryFilter: QueryArgument {
    // WHERE
    override init(_ whereq: String) {
        super.init("WHERE")
        self.paramsString = whereq
    }
}

class QueryOrder: QueryArgument {
    // ORDER BY
    override init(_ order: String) {
        super.init("ORDER BY")
        self.paramsString = order
    }
}

class QueryParser {
    private var order: [String: Int] = [:]
    var argOrder: [String: Int] {
        get {
            for i in 0...args.count-1 {
                order[args[i].argName] = i
            }
            return order
        }
    }
    var args: [QueryArgument] = []
    
    func buildSQLCommand() -> String {
        var sql = ""
        for item in args {
            sql += item.sqlString + " "
        }
        sql += ";"
        return sql
    }
    
    subscript(arg: Int) -> QueryArgument {
        return args[ arg ]
    }
    
    subscript(arg: String) -> QueryArgument {
        return args[ argOrder[arg]! ]
    }
    
    init(select: String, from: String, filter: String, sort: String) {
        if select != "" {
            args.append(QuerySelection(select))
        } else {
            args.append(QuerySelection("*"))
        }
        if from != "" {
            args.append(QueryFrom(from))
        } else {
            print("You must specify a table when querying the database. Please try again.")
            exit(0)
        }
        if filter != "" {
            args.append(QueryFilter(filter))
        }
        if sort != "" {
            args.append(QueryOrder(sort))
        }
    }
    convenience init(_ from: String) {
        self.init(select: "", from: from, filter: "", sort: "")
    }
    convenience init(_ select: String, from: String) {
        self.init(select: select, from: from, filter: "", sort: "")
    }
}
