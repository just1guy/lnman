import Foundation
import Commander
import Pathos
import SwiftyTextTable
import SwiftShell

func renderTables(_ rows: [[String]], headers: [String], title: String) -> String {
    var cols: [TextTableColumn] = []
    for item in headers {
        cols.append(TextTableColumn(header: item))
    }
    var table = TextTable(columns: cols)
    
    table.addRows(values: rows)
    
    if title != "" {
        table.header = title
    }
    return table.render()
}

let file = try! expandUserDirectory(inPath: "~/.config/lnman/links.sqlite")

let main = Group {
    $0.command(
        "list",
        Option("select", default: "name,source,target,description,linktype", description: "Columns to select"),
        Option("filter", default: "id>0", description: "Filter to use"),
        Option("sort", default: "name", description: "Sort a specific column")
    ) { select, filter, sort in
        let query = QueryParser(select: select, from: "links", filter: filter, sort: sort)
        let db = DB(file)
        print( renderTables(db.getRows(query: query.buildSQLCommand()), headers: query["SELECT"].paramsArray, title: "Links") )
    }

    $0.command(
        "new",
        Argument<String>("name", description: "Name of the link"),
        Option("description", default: "", description: "Description of the link"),
        Argument<String>("source", description: "The original file"),
        Argument<String>("target", description: "Path to new link"),
        Option("type", default: "symbolic", description: "Type of link [symbolic, hard]")
    ) { name, description, source, target, type in
        let result = run(bash: "ln \(type == "symbolic" ? "-s" : "") \(source) \(target)")
        if (result.exitcode != 0) {
            print(result.exitcode)
            exit(0)
        } else {
            print("Link created successfully")
        }
        let db = DB(file)
        let rowid = db.addEntry(
            query: "INSERT INTO links (name, description, source, target, linktype) VALUES (\"\(name)\", \"\(description)\", \"\(source)\", \"\(target)\", \"\(type)\")")
        print("Database updated")
        print(renderTables(db.getRows(query: "SELECT name, description, source, target, linktype FROM links WHERE id=\(rowid)"), headers: ["name", "description", "source", "target", "linktype"], title: "New Link"))
    }
    
    $0.command(
        "delete",
        Argument<String>("name", description: "Name of the link to delete")
        ) { name in
            let db = DB(file)
            let result = db.getRow(query: "SELECT name, target, id FROM links WHERE name=\"\(name)\"")
            print(result["target"])
            let ayyy = run(bash: "rm -r \(result["target"] ?? "")")
            if ayyy.exitcode != 0 {
                    print("There was a problem deleting the link")
                exit(0)
            }
            
            try! db.dbQueue.write { db in
                try! db.execute(sql: "DELETE FROM links WHERE id=\(result["id"]!)")
            }
    }
    

}

initialize()
main.run()
