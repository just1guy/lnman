//
//  initialization.swift
//  
//
//  Created by Robert Sale on 11/23/19.
//

import Foundation
import Pathos

func initialize() {
    if !exists( atPath: try! expandUserDirectory( inPath: "~/.config/lnman" ) ) {
        do {
            try createDirectory( atPath: try! expandUserDirectory( inPath: "~/.config/lnman" ), createParents: true )
        } catch {
            print("Unable to create config folder. Make sure you have permission to write to \( try! expandUserDirectory( inPath: "~/.config/" ) ) and try again.")
        }
    }
}

let listFilePath = try! expandUserDirectory(inPath: "~/.config/lnman/links.sqlite")
