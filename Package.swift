// swift-tools-version:5.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "lnman",
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(
            url: "https://github.com/kylef/Commander",
            from: "0.9.0"
        ),
        .package(
            url: "https://github.com/groue/GRDB.swift.git",
            from: "4.0.0"
        ),
        .package(
            url: "https://github.com/dduan/Pathos", 
            from: "0.2.1"
        ),
        .package(
            url: "https://github.com/scottrhoyt/SwiftyTextTable.git",
            from: "0.5.0"
        ),
        .package(
            url: "https://github.com/kareman/SwiftShell",
            from: "5.0.1"
        )
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "lnman",
            dependencies: ["Commander", "GRDB", "Pathos", "SwiftyTextTable", "SwiftShell"]),
        .testTarget(
            name: "lnmanTests",
            dependencies: ["lnman"]),
    ]
)
