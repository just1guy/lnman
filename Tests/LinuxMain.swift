import XCTest

import lnmanTests

var tests = [XCTestCaseEntry]()
tests += lnmanTests.allTests()
XCTMain(tests)
